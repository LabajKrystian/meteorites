//
//  NetworkUtils.swift
//  Meteorites
//
//  Created by Krystian Labaj on 27/08/2020.
//

import Foundation
import Alamofire

class NetworkUtils {
    static func sendRequest<T: Decodable, E: Decodable>(_ url: URLConvertible,
                                        onSuccess handleResult: @escaping (T) -> Void,
                                        onError handleError: ((Int?, E?) -> Void)? = nil,
                                        method: HTTPMethod = .get,
                                        parameters: [String:Any]? = nil,
                                        headers: [String:String]? = nil) {
        var afHeaders = HTTPHeaders()

        headers?.forEach { (header) in
            let (key, value) = header
            afHeaders.add(name: key, value: value)
        }

        AF.request(url, method: method, parameters: parameters, headers: afHeaders.isEmpty ? nil : afHeaders).validate().responseDecodable { (response: DataResponse<T, AFError>) in
            switch response.result {
            case .success(let data):
                handleResult(data)
            case .failure(let errorData):
                if let data = response.data {
                    do {
                        let json = try JSONDecoder().decode(E.self, from: data)
                        handleError?(errorData.responseCode, json)
                    } catch {
                        print("json error: \(error)")
                    }
                }
            }
        }
    }
}
