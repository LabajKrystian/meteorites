//
//  UIStoryboardExtension.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18/10/2020.
//

import UIKit

extension UIStoryboard {
    
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.filename, bundle: bundle)
    }
    
    enum Storyboard: String {
        case meteorite = "Meteorite"
        case meteoriteDetail = "MeteoriteDetail"
        
        var filename: String {
            return rawValue
        }
    }
}
