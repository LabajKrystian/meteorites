//
//  DateFormatter.swift
//  Meteorites
//
//  Created by Krystian Labaj on 03.01.2021.
//

import UIKit

extension DateFormatter {
    func formatDate(_ stringDate: String) -> String {
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        self.timeZone = TimeZone(abbreviation: "CET")
        guard let date = self.date(from: stringDate) else { return "" }
        self.dateFormat = "yyyy"
        return self.string(from: date)
    }
}
