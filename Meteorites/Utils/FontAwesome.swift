//
//  FontAwesome.swift
//  Meteorites
//
//  Created by Krystian Labaj on 03.01.2021.
//

import Foundation

func getFontAwesomeIconNameAndFontName(iconInfo: String?) -> (String, String) {
    var iconName = ""
    var iconFontName = "FontAwesome5Pro-Regular"

    if let icon = iconInfo {
        let iconPattern = #"^fa(?<style>[srldb]) fa-(?<iconName>.*)$"#
        let regex = try? NSRegularExpression(pattern: iconPattern, options: [])
        var nsrange = NSRange(icon.startIndex..<icon.endIndex, in: icon)

        if let match = regex?.firstMatch(in: icon, options: [], range: nsrange) {
            nsrange = match.range(withName: "style")

            if nsrange.location != NSNotFound,
                let range = Range(nsrange, in: icon) {
                let iconStyle = String(icon[range])

                switch iconStyle {
                case "s": iconFontName = "FontAwesome5Pro-Solid"
                case "r": iconFontName = "FontAwesome5Pro-Regular"
                case "l": iconFontName = "FontAwesome5Pro-Light"
                default: iconFontName = "FontAwesome5Pro-Regular"
                }
            }

            nsrange = match.range(withName: "iconName")

            if nsrange.location != NSNotFound,
                let range = Range(nsrange, in: icon) {
                iconName = String(icon[range])
            }
        }
    }

    return (iconName, iconFontName)
}
