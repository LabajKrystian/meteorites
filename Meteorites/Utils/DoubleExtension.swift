//
//  DoubleExtension.swift
//  Meteorites
//
//  Created by Krystian Labaj on 25.08.2021.
//

import Foundation

extension Double {
    func truncate(places : Int)-> Double {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}
