//
//  Meteorite.swift
//  Meteorites
//
//  Created by Krystian Labaj on 15/10/2020.
//

import Foundation

struct Meteorite: Codable {
    var id: String
    var name: String
    var mass: String?
    var year: String?
    var fall: String?
    var recclass: String?
    var reclong: String?
    var reclat: String?
}
