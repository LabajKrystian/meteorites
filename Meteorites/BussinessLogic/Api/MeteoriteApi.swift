//
//  MeteoriteApi.swift
//  Meteorites
//
//  Created by Krystian Labaj on 15/10/2020.
//

import Foundation

class MeteoriteApi {
    
    private let BASE_URL = "https://data.nasa.gov/resource/gh4g-9sfh.json?"
    
    func getMeteorite(offset: Int, onSuccess handleResult: @escaping ([Meteorite]) -> Void, onError handleError: ((Int?, [Meteorite]?) -> Void)? = nil) {
        NetworkUtils.sendRequest("\(BASE_URL)$limit=20&$offset=\(offset)", onSuccess: handleResult, onError: handleError, headers: defaultHeaders())
    }
    
    private func defaultHeaders() -> [String:String] {
        var headers = [String:String]()
        headers = ["X-App-Token" : "4vasb6c5AXibFstAfrzhrBD3H"]
        return headers
    }
}
