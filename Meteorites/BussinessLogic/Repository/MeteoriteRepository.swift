//
//  MeteoriteRepository.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18/10/2020.
//

import UIKit

protocol MeteoriteRepository {
    func getMeteorite(offset: Int, onError handleError: ((Int?, [Meteorite]) -> Void)?, onSuccess handleResult: @escaping ([Meteorite]) -> Void)
}

extension MeteoriteRepository {
    func getMeteorite(offset: Int, onError handleError: ((Int?, [Meteorite]) -> Void)? = nil, onSuccess handleResult: @escaping ([Meteorite]) -> Void) {
        getMeteorite(offset: offset, onError: handleError, onSuccess: handleResult)
    }
}

struct ApiMeteoriteRepository: MeteoriteRepository {
    let api: MeteoriteApi
    
    func getMeteorite(offset: Int, onError handleError: ((Int?, [Meteorite]) -> Void)? = nil, onSuccess handleResult: @escaping ([Meteorite]) -> Void) {
        let handleResult = { (response: [Meteorite])  in handleResult(response) }
        let handleError = { (statusCode: Int?, errorResponse: [Meteorite]?) in
            if handleError != nil {
                handleError!(statusCode, errorResponse!)
            }
        }
        api.getMeteorite(offset: offset, onSuccess: handleResult, onError: handleError)
    }
}

let meteoriteRepository = ApiMeteoriteRepository(api: MeteoriteApi())
