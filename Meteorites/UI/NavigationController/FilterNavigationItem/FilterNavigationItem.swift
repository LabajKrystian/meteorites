//
//  FilterNavigationItem.swift
//  Meteorites
//
//  Created by Krystian Labaj on 03.01.2021.
//

import UIKit

class FilterNavigationItem: UIView {
    
    @IBOutlet var view: UIView!
    
    var isOpened = false
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        createView()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        
         createView()
    }
    
    private func createView() {
        Bundle.main.loadNibNamed(String(describing: FilterNavigationItem.self), owner: self, options: nil)
        addSubview(view)
        
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
}
