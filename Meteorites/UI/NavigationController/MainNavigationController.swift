//
//  MainNavigationController.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18/10/2020.
//

import UIKit

class MainNavigationController: UINavigationController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createView()
    }
    
    private func createView() {
        let storyboard = UIStoryboard(storyboard: .meteorite)
        if let viewController = storyboard.instantiateInitialViewController() {
            self.viewControllers = [viewController]
        }
    }
}
