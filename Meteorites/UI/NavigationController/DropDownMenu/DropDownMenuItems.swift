//
//  DropDownMenuItems.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18.01.2021.
//

import UIKit

enum DropDownMenuItems {
    case sortByYear
    case sortByMass
}
