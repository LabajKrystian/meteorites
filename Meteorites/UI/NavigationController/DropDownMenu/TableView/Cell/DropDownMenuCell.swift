//
//  DropDownMenuCell.swift
//  Meteorites
//
//  Created by Krystian Labaj on 03.01.2021.
//

import UIKit

class DropDownMenuCell: UITableViewCell {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var dropDownOptionLabel: UILabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createView()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createView()
    }

    private func createView() {
        Bundle.main.loadNibNamed(String(describing: DropDownMenuCell.self), owner: self, options: nil)
        addSubview(view)
        
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func bind(optionTitle: String) {
        dropDownOptionLabel.text = optionTitle
    }
}
