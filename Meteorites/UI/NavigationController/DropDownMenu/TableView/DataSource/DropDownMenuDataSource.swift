//
//  DropDownMenuDataSource.swift
//  Meteorites
//
//  Created by Krystian Labaj on 03.01.2021.
//

import UIKit

protocol DropDownMenuDataSourceDelegate: class {
    func dropDownMenuDataSource(_ dataSource: DropDownMenuDataSource, didSelectItem item: DropDownMenuItems)
}

class DropDownMenuDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var items: [DropDownMenuItems] = []
    
    weak var delegate: DropDownMenuDataSourceDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DropDownMenuCell.self),
                                                 for: indexPath)
        if let cell = cell as? DropDownMenuCell {
            switch items[indexPath.row] {
            case .sortByMass:
                cell.bind(optionTitle: "Sort by mass")
            case .sortByYear:
                cell.bind(optionTitle: "Sort by year")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.dropDownMenuDataSource(self, didSelectItem: items[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
