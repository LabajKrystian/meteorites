//
//  DropDownMenu.swift
//  Meteorites
//
//  Created by Krystian Labaj on 03.01.2021.
//

import UIKit

protocol DropDownMenuDelegate: class {
    func dropDownMenu(_ view: DropDownMenu, selectedItem item: DropDownMenuItems)
}

class DropDownMenu: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var dropDownTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: DropDownMenuDelegate?
    
    private var showDropDownMenuAnimation: UIViewPropertyAnimator?
    private var hideDropDownMenuAnimation: UIViewPropertyAnimator?
    
    private var items: [DropDownMenuItems] = []
    private var isOpened = false
    
    private let dataSource = DropDownMenuDataSource()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        createView()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        
         createView()
    }
    
    private func createView() {
        Bundle.main.loadNibNamed(String(describing: DropDownMenu.self), owner: self, options: nil)
        addSubview(view)
        
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        setupTableView()
        setupDropDownMenuActions()
    }
    
    private func setupTableView() {
        dropDownTableView.register(DropDownMenuCell.self,
                                   forCellReuseIdentifier: String(describing: DropDownMenuCell.self))
        dropDownTableView.delegate = dataSource
        dropDownTableView.dataSource = dataSource
        dataSource.delegate = self
    }
    
    private func setupDropDownMenuActions() {
        items = [.sortByMass, .sortByYear]
        dataSource.items = items
        dropDownTableView.reloadData()
    }
    
    func showDropDownMenu() {
        isOpened = true
        dropDownTableView.frame.origin.y = -tableViewHeightConstraint.constant
        showDropDownMenuAnimation = UIViewPropertyAnimator(duration: 0.25, curve: .easeOut, animations: { [weak self] in
            guard let self = self else { return }
            self.view.isHidden = false
            self.view.alpha = 1
            self.dropDownTableView.frame.origin.y = 0
        })
        
        showDropDownMenuAnimation?.startAnimation()
    }
    
    func hideDropDownMenu() {
        isOpened = false
        hideDropDownMenuAnimation = UIViewPropertyAnimator(duration: 0.25, curve: .easeOut, animations: { [weak self] in
            guard let self = self else { return }
            self.view.alpha = 0
            self.dropDownTableView.frame.origin.y = -self.tableViewHeightConstraint.constant
        })
        
        hideDropDownMenuAnimation?.addCompletion({ _ in
            if !self.isOpened {
                self.view.isHidden = true
            }
        })
        
        hideDropDownMenuAnimation?.startAnimation()
    }
}

extension DropDownMenu: DropDownMenuDataSourceDelegate {
    func dropDownMenuDataSource(_ dataSource: DropDownMenuDataSource, didSelectItem item: DropDownMenuItems) {
        delegate?.dropDownMenu(self, selectedItem: item)
    }
}
