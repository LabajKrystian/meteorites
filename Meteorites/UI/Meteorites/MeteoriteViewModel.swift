//
//  MeteoriteViewModel.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18/10/2020.
//

import RxSwift

class MeteoriteViewModel {
    let repository: MeteoriteRepository = meteoriteRepository
    
    var viewState: BehaviorSubject<MeteoriteViewState>
    
    init() {
        viewState = BehaviorSubject(value: MeteoriteViewState(loading: false, data: [], page: 0, offset: 0, hasMoreData: true))
        getMeteorites()
    }
    
    func getMeteorites() {
        if var viewState = try? self.viewState.value() {
            viewState.loading = true
            viewState.offset = 0
            viewState.page = 0
            self.viewState.onNext(viewState)
        }
        
        self.repository.getMeteorite(offset: 0) { data in
            if var viewState = try? self.viewState.value() {
                viewState.loading = false
                viewState.data = data
                viewState.page = 1
                viewState.offset = viewState.offset + data.count
                viewState.hasMoreData = data.count > 0
                self.viewState.onNext(viewState)
            }
        }
    }
    
    func getMoreMeteorites(offset: Int) {
        let state = try? self.viewState.value()
        if let state = state {
            if !state.loading && state.hasMoreData {
                if var viewState = try? self.viewState.value() {
                    viewState.loading = true
                    self.viewState.onNext(viewState)
                }
                
                self.repository.getMeteorite(offset: offset) { data in
                    if var viewState = try? self.viewState.value() {
                        let meteorites = state.data + data
                        viewState.loading = false
                        viewState.data = meteorites
                        viewState.page = state.page + 1
                        viewState.offset = offset + data.count
                        viewState.hasMoreData = data.count > 0
                        self.viewState.onNext(viewState)
                    }
                }
            }

        }
    }
}
