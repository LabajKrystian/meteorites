//
//  MeteoriteViewState.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18/10/2020.
//

import UIKit

struct MeteoriteViewState {
    var loading: Bool
    var data: [Meteorite]
    var page: Int
    var offset: Int
    var hasMoreData: Bool
}
