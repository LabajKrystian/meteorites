//
//  HomeDataSource.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18/10/2020.
//

import UIKit

protocol MeteoriteDataSourceDelegate: class {
    func meteoriteDataSource(_ dataSource: MeteoriteDataSource, didSelectMeteorite meteorite: Meteorite)
    func onBottomReached(_ dataSource: MeteoriteDataSource, items: Int)
}

class MeteoriteDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private let lazyLoadOffset = 10
    
    var delegate: MeteoriteDataSourceDelegate?
    var meteorites: [Meteorite] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meteorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MeteoriteCell.self), for: indexPath)
        let meteorite = meteorites[indexPath.row]
        if let cell = cell as? MeteoriteCell {
            cell.bind(name: meteorite.name,
                      year: meteorite.year ?? "",
                      mass: meteorite.mass ?? ""
            )
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.meteoriteDataSource(self, didSelectMeteorite: meteorites[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= meteorites.count - lazyLoadOffset {
            delegate?.onBottomReached(self, items: meteorites.count)
        }
    }
}
