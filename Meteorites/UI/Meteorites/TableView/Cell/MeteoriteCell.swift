//
//  MeteoriteCell.swift
//  Meteorites
//
//  Created by Krystian Labaj on 18/10/2020.
//

import UIKit

class MeteoriteCell: UITableViewCell {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createView()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createView()
    }

    private func createView() {
        Bundle.main.loadNibNamed(String(describing: MeteoriteCell.self), owner: self, options: nil)
        addSubview(view)
        
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func bind (name: String, year: String, mass: String) {
        nameLabel.text = name
        
        let mass = Double(mass) ?? 0
        massLabel.text = "mass: \(mass.truncate(places: 3)) g"
        
        let dateFormatter = DateFormatter()
        yearLabel.text = "year: \(dateFormatter.formatDate(year))"
    }
}
