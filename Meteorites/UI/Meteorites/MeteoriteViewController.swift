//
//  MeteoriteViewController.swift
//  Meteorites
//
//  Created by Krystian Labaj on 26/08/2020.
//

import UIKit
import RxSwift

class MeteoriteViewController: UIViewController {
    
    @IBOutlet weak var meteoriteTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var dropDownMenu: DropDownMenu!
    
    private let dataSource = MeteoriteDataSource()
    private let viewModel = MeteoriteViewModel()
    private let disposeBag = DisposeBag()
    private let filterNavigationItem = FilterNavigationItem()
    private let refreshControl = UIRefreshControl()
    
    private var meteorites: [Meteorite] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Meteorites"
        setupTableView()
        setupNavigationItem()
        
        viewModel.viewState.subscribe(onNext: { [weak self] (viewState) in
            guard let self = self else { return }
            self.render(viewState: viewState)
        })
        .disposed(by: disposeBag)
    }
    
    func render(viewState: MeteoriteViewState) {
        activityIndicator.isHidden = !viewState.loading
        setItems(viewState.data)
    }
    
    private func setItems(_ items: [Meteorite]) {
        meteorites = items
        dataSource.meteorites = items
        meteoriteTableView.reloadData()
    }
    
    private func setupTableView() {
        meteoriteTableView.register(MeteoriteCell.self, forCellReuseIdentifier: String(describing: MeteoriteCell.self))
        meteoriteTableView.dataSource = dataSource
        meteoriteTableView.delegate = dataSource
        dataSource.delegate = self
        setupRefreshControl()
    }
    
    private func setupRefreshControl() {
        meteoriteTableView.refreshControl = refreshControl
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .systemYellow
    }
    
    private func setupNavigationItem() {
        let rightBarButtonItem = UIBarButtonItem(customView: filterNavigationItem)
        navigationItem.rightBarButtonItem = rightBarButtonItem
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(filterTapped))
        navigationController?.navigationBar.addGestureRecognizer(tapGesture)
        dropDownMenu.delegate = self
    }
    
    private func openMeteoriteDetail(meteorite: Meteorite) {
        let storyboard = UIStoryboard(storyboard: .meteoriteDetail)
        let viewController = storyboard.instantiateInitialViewController()
        viewController?.modalPresentationStyle = .popover
        
        if let detailViewController = viewController as? MeteoriteDetailViewController {
            detailViewController.meteorite = meteorite
            present(detailViewController, animated: true, completion: nil)
        }
    }
    
    @objc func refresh() {
        viewModel.getMeteorites()
        refreshControl.endRefreshing()
    }
    
    @objc func filterTapped() {
        filterNavigationItem.isOpened = !filterNavigationItem.isOpened
        
        if filterNavigationItem.isOpened {
            dropDownMenu.showDropDownMenu()
            dropDownMenu.isHidden = false
        } else {
            dropDownMenu.hideDropDownMenu()
            dropDownMenu.isHidden = true
        }
    }
}

extension MeteoriteViewController: MeteoriteDataSourceDelegate {
    func onBottomReached(_ dataSource: MeteoriteDataSource, items: Int) {
        viewModel.getMoreMeteorites(offset: items)
    }
    
    func meteoriteDataSource(_ dataSource: MeteoriteDataSource, didSelectMeteorite meteorite: Meteorite) {
        openMeteoriteDetail(meteorite: meteorite)
    }
}

extension MeteoriteViewController: DropDownMenuDelegate {
    func dropDownMenu(_ view: DropDownMenu, selectedItem item: DropDownMenuItems) {
        switch item {
        case .sortByMass:
            meteorites.sort { $0.mass ?? "" > $1.mass ?? "" }
            dataSource.meteorites = meteorites
            filterTapped()
            meteoriteTableView.reloadData()
        case .sortByYear:
            meteorites.sort { $0.year ?? "" > $1.year ?? "" }
            dataSource.meteorites = meteorites
            filterTapped()
            meteoriteTableView.reloadData()
        }
    }
}
