//
//  MeteoriteDetailViewController.swift
//  Meteorites
//
//  Created by Krystian Labaj on 02.01.2021.
//

import UIKit
import MapKit

class MeteoriteDetailViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var fallLabel: UILabel!
    @IBOutlet weak var reclassLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    
    var meteorite: Meteorite?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCenterCoordinates()
        bind()
    }
    
    private func setupCenterCoordinates() {
        if let meteorite = meteorite,
           let longitude = CLLocationDegrees(meteorite.reclong ?? ""),
           let latitude = CLLocationDegrees(meteorite.reclat ?? "") {
            
            let centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
            let annotation = MKPointAnnotation()
            annotation.coordinate = centerCoordinate
            mapView.addAnnotation(annotation)
            mapView.setCenter(centerCoordinate, animated: true)
        }
    }
    
    func bind() {
        if let meteorite = meteorite {
            nameLabel.text = meteorite.name
            idLabel.text = "id: \(meteorite.id)"
            
            let mass = Double(meteorite.mass ?? "0") ?? 0
            massLabel.text = "mass: \(mass.truncate(places: 3)) g"
            let dateFormatter = DateFormatter()
            yearLabel.text = "year: \(dateFormatter.formatDate(meteorite.year ?? ""))"
            fallLabel.text = "fall: \(meteorite.fall ?? "")"
            reclassLabel.text = "recc: \(meteorite.recclass ?? "")"
            longitudeLabel.text = "long: \(meteorite.reclong ?? "")"
            latitudeLabel.text = "lat: \(meteorite.reclat ?? "")"
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
